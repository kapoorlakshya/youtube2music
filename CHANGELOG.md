### 0.2.0 (TBD)

* Add support for Firefox and Edge.
* Add support for timestamp (`t=27`) and playlist (`list=`) values in the URL.
* Failing to extract required information from the link in focus results
in an alert informing the user about the failure. The error points a link
to the GitHub repo where the issue can be reported.

### 0.1.0 (2019-06-04)

* Released Chrome extension with support for `youtube.com` and `youtu.be` links.